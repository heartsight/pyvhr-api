# Use an official CUDA runtime as a parent image
FROM nvidia/cuda:11.5.0-base-ubuntu20.04

# Set the working directory to /app
WORKDIR /app

# Set the environment variable to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

# Install wget, Fortran compiler, and required system libraries
RUN apt-get update && \
    apt-get install -y wget && \
    apt-get install -y gfortran && \
    apt-get install -y libgl1-mesa-glx && \
    apt-get install -y libglib2.0-0

# Not needed for cpu version
# Install the CUDA Toolkit and CuDNN
RUN apt-get install -y --no-install-recommends libnvidia-gl-510 && \
    apt-get install -y --no-install-recommends cuda-toolkit-11-5 && \
    apt-get install -y --no-install-recommends libcudnn8-dev  && \
    apt-get install -y --no-install-recommends nvidia-driver-495 && \
    rm -rf /var/lib/apt/lists/*

# Install Miniconda
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /miniconda.sh && \
    chmod +x /miniconda.sh && \
    /miniconda.sh -b -p /opt/conda && \
    rm /miniconda.sh
ENV PATH="/opt/conda/bin:${PATH}"

# Copy the Conda environment file to the container
COPY environment.yml .

# Create the Conda environment
RUN conda env create -f environment.yml

# Copy the rest of the application files to the container
COPY . .

# Specify the command to run the application
CMD [ "/bin/bash", "-c", "source activate pyvhr && python ./program.py" ]
