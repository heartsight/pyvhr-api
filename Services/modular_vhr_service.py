from Utilities.pipeline_util import Pipeline
from Utilities.landmark_util import *
from pyVHR.utils.errors import getErrors, printErrors, displayErrors


def process_landmarks(environment, sig_processing, sig):
    """
    Takes a video and extracts average heart rate using pyVHR algorithms

    :param environment: dictionary containing various pipeline variables:
        - win_size: int: Window size in seconds
        - bpm_est: string: BPM final estimate, if patches choose 'medians' or 'clustering'
        - method: string: one of the methods implemented in pyVHR
        - patch_size: size of patch to be extracted

    :param sig_processing: SignalProcessing class
    :param sig: np.array(sig, dtype=np.float32)

    :return: tuple: (bvps, timesES, bpmES)
    """

    # run
    pipe = Pipeline()  # object to execute the pipeline
    bvps, timesES, bpmES = pipe.run_on_landmark(sig_processing,
                                                sig,
                                             winsize=environment['video_duration'],
                                             fps=environment['fps'],
                                             method=environment['method'],
                                             estimate=environment['bpm_est'],
                                             RGB_LOW_HIGH_TH=(5, 230),
                                             Skin_LOW_HIGH_TH=(5, 230),
                                             pre_filt=True,
                                             post_filt=True,
                                             cuda=True,
                                             verb=True)

    print(f"Completed, result: {bpmES[0]}")

    return bpmES[0]





def extract_landmarks(video_path, environment):
    landmark_list = [2, 3, 4, 5, 6, 8, 9, 10, 18, 21, 32, 35, 36, 43, 46, 47, 48, 50, 54, 58, 67, 68, 69, 71, 92, 93,
                     101, 103, 104, 108, 109, 116, 117, 118, 123, 132, 134, 135, 138, 139, 142, 148, 149, 150, 151,
                     152, 182, 187, 188, 193, 197, 201, 205, 206, 207, 210, 211, 212, 216, 234, 248, 251, 262, 265,
                     266, 273, 277, 278, 280, 284, 288, 297, 299, 322, 323, 330, 332, 333, 337, 338, 345, 346, 361,
                     363, 364, 367, 368, 371, 377, 379, 411, 412, 417, 421, 425, 426, 427, 430, 432, 436]

    sig_processing = SignalProcessing()
    sig_processing.set_landmarks(landmark_list)
    sig_processing.set_square_patches_side(np.float(environment['patch_size']))
    sig = sig_processing.extract_patches(video_path, 'squares', 'mean')

    return process_landmarks(environment, sig_processing, sig)
