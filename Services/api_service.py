from flask import Flask, request, jsonify
from threading import Thread
import Services.vhr_service as py_vhr
import Utilities.files_util as files
import Utilities.environment_util as environment
import requests
import json

app = Flask(__name__)


def process_video_file(video_request):
    try:
        env = environment.process_environment_video(video_request['video_path'])  # Analyze the video to determine metrics
        heart_rate = py_vhr.process_video(video_request['video_path'], env)  # Process the video file using pyVHR
        files.remove_video(video_request['video_path'])  # Remove the video from server after processing
        data = {
            'Pulse': round(heart_rate.item()),
            'UserId': video_request['user_id'],
            'DeviceId': video_request['device_id'],
            'DeviceType': video_request['device_type']
        }

        json_data = json.dumps(data)  # Convert the dictionary to a JSON string

        print(json_data)

        response = requests.post('https://pulse.heartvision.cloud/Pulse/process',
                                 data=json_data,
                                 headers={'Content-Type': 'application/json'},
                                 verify=False)

        if response.status_code != 200:
            print(f"Error sending heart rate to Pulse API: {response.text}")
        else:
            print(f"Request successfully sent to Pulse API {response.text}")

    except Exception as ex:
        print(f"Exception occurred in processing thread: {ex}")


@app.route("/analyze", methods=['POST'])
def analyze():
    try:
        print("Request received.")
        video_path = files.save_video(request.files['video'])

        # Retrieve the parameters from post request
        video_request = {'video_path': video_path,
                         'user_id': request.form['user_id'],
                         'device_id': request.form['device_id'],
                         'device_type': request.form['device_type']}

        print("Beginning thread")
        # Process the video file asynchronously using pyVHR in a separate thread
        Thread(target=process_video_file, args=(video_request,)).start()

        return {'status': 'success', 'message': 'Video is being processed.'}

    except Exception as ex:
        # If an exception occurs, return an error response
        print(f"Exception occurred: {ex}")

        return {'status': 'error', 'message': str(ex)}


app.run(host='0.0.0.0', port=80)
