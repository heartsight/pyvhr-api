import csv

from pyVHR.analysis.pipeline import Pipeline
from pyVHR.plot.visualize import *
import os
from pyVHR.utils.errors import getErrors, printErrors, displayErrors


def get_heart_rate(video_path, rppg_method):
    videoFileName = video_path  # video file name
    video = cv2.VideoCapture(videoFileName)
    fps = video.get(cv2.CAP_PROP_FPS)  # Frames per second
    frame_count = int(video.get(cv2.CAP_PROP_FRAME_COUNT))  # Total number of frames
    video_duration = frame_count / fps  # Duration in seconds

    # params
    wsize = video_duration  # window size in seconds
    roi_approach = 'patches'  # 'holistic' or 'patches'
    bpm_est = 'clustering'  # BPM final estimate, if patches choose 'medians' or 'clustering'
    method = rppg_method  # one of the methods implemented in pyVHR
    patch_size = 16

    # run
    pipe = Pipeline()  # object to execute the pipeline
    bvps, timesES, bpmES = pipe.run_on_video(videoFileName,
                                             winsize=wsize,
                                             roi_method='convexhull',
                                             roi_approach=roi_approach,
                                             method=method,
                                             estimate=bpm_est,
                                             patch_size=patch_size,
                                             RGB_LOW_HIGH_TH=(5, 230),
                                             Skin_LOW_HIGH_TH=(5, 230),
                                             pre_filt=True,
                                             post_filt=True,
                                             cuda=True,
                                             verb=False)

    return bpmES


def main():
    directory_path = R'..\uploads'

    # Get a list of all files in the directory
    all_files = os.listdir(directory_path)

    # Filter the list to keep only the MP4 files
    mp4_files = [file for file in all_files if file.lower().endswith('.mp4')]

    video_results = {}

    # Available methods: ['cpu_GREEN', 'cpu_CHROM', 'cpu_LGI', 'cpu_PBV', 'cpu_PCA', 'cpu_POS', 'cpu_OMIT']
    methods = methods = ['cpu_GREEN', 'cpu_CHROM', 'cpu_LGI', 'cpu_PBV', 'cpu_PCA', 'cpu_POS', 'cpu_OMIT']

    # Iterate through the MP4 files
    for mp4_file in mp4_files:
        # Create the full path to the MP4 file
        video_path = os.path.join(directory_path, mp4_file)
        video_name = os.path.basename(video_path)

        print(f"Processing {video_name}")

        for method in methods:
            result_array = get_heart_rate(video_path, method)

            # Convert the array to a float and average it if there are multiple values
            result = np.mean(result_array)

            # Remove the .mp4 extension from the video name
            video_name_no_ext = os.path.splitext(video_name)[0]

            # Initialize the dictionary for the video if it's not already present
            if video_name_no_ext not in video_results:
                video_results[video_name_no_ext] = {}

            # Store the result in the dictionary
            video_results[video_name_no_ext][method] = result

            print(f"{method}: {result}")

    # Print the results
    print("\nVideo processing results:")
    for video_name, method_results in video_results.items():
        print(f"{video_name}:")
        for method, result in method_results.items():
            print(f"  {method}: {result}")

    # Write the results to a CSV file
    with open('video_results.csv', 'w', newline='') as csvfile:
        fieldnames = ['Video Name'] + methods
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for video_name, method_results in video_results.items():
            row_dict = {'Video Name': video_name}
            row_dict.update(method_results)
            writer.writerow(row_dict)


main()
