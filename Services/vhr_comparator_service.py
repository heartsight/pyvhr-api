from vhr_service import *
from modular_vhr_service import *
from Utilities.environment_util import *


def main():
    video_path = R'D:\School\Heart Vision\Test Videos\20230607_193737.mp4'

    print("starting original")
    environment2 = process_environment_video(video_path)
    original_result = process_video(video_path, environment2)

    print("starting modular")
    environment = process_environment_landmark(video_path)
    modular_result = extract_landmarks(video_path, environment)

    print(f"modular result: {modular_result}")
    print(f"original result: {original_result}")


main()
