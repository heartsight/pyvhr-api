from pyVHR.analysis.pipeline import Pipeline
from pyVHR.plot.visualize import *
from pyVHR.utils.errors import getErrors, printErrors, displayErrors


def process_video(video, environment):
    """
    Takes a video and extracts average heart rate using pyVHR algorithms

    :param video: file object: Video to be analyzed
    :param environment: dictionary containing various pipeline variables:
        - win_size: int: Window size in seconds
        - roi_approach: string: 'holistic' or 'patches'
        - bpm_est: string: BPM final estimate, if patches choose 'medians' or 'clustering'
        - method: string: one of the methods implemented in pyVHR

    :return: tuple: (bvps, timesES, bpmES)
    """

    # run
    pipe = Pipeline()  # object to execute the pipeline
    bvps, timesES, bpmES = pipe.run_on_video(video,
                                             winsize=environment['video_duration'],
                                             roi_method='convexhull',
                                             roi_approach=environment['roi_approach'],
                                             method=environment['method'],
                                             estimate=environment['bpm_est'],
                                             patch_size=32,
                                             RGB_LOW_HIGH_TH=(5, 230),
                                             Skin_LOW_HIGH_TH=(5, 230),
                                             pre_filt=True,
                                             post_filt=True,
                                             cuda=True,
                                             verb=True)

    print(f"Completed, result: {bpmES[0]}")

    return bpmES[0]
