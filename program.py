import Utilities.files_util as files
from Services.api_service import app

def main():
    # Creates temp directory for folders
    print("Initializing folders...")
    files.initialize()
    print("Folders Initialized")

    # Starts API
    print("Starting app...")
    app.run()


if __name__ == "__main__":
    main()
