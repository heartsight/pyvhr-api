import os

UPLOAD_FOLDER = 'uploads'


def initialize():
    if not os.path.exists(UPLOAD_FOLDER):
        os.makedirs(UPLOAD_FOLDER)


import base64

def save_video(video):
    # Get the filename (this assumes it was included in the request)
    print("Saving video...")
    video_filename = os.path.join(UPLOAD_FOLDER, video.filename)
    print("Creating path.")
    video.save(video_filename)
    print("Saved video!")

    return video_filename


def remove_video(video_file_name):
    os.remove(video_file_name)
