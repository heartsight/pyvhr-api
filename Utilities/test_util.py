import os
import cv2
from video_util import reduce_resolution, reduce_frames


def process_directory(directory_path):
    # Get a list of all files in the directory
    all_files = os.listdir(directory_path)

    # Filter the list to keep only the MP4 files
    mp4_files = [file for file in all_files if file.lower().endswith('.mp4')]

    # Iterate through the MP4 files
    for mp4_file in mp4_files:
        # Create the full paths for input and output files
        input_path = os.path.join(directory_path, mp4_file)
        output_path_resoution = os.path.join(directory_path, os.path.splitext(mp4_file)[0] + '_res.mp4')
        output_path_frames = os.path.join(directory_path, os.path.splitext(mp4_file)[0] + '_fr.mp4')

        # Process the video and create a copy with half the resolution
        reduce_resolution(input_path, output_path_resoution)
        reduce_frames(input_path, output_path_frames)

# Run the script
directory_path = R'../uploads'
process_directory(directory_path)
