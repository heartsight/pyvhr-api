import numpy as np
import pandas as pd
from pyVHR.BPM.BPM import BVPsignal


def extract_bpm_from_csv(csv_file, sample_rate):
    # Read the CSV file into a pandas DataFrame
    df = pd.read_csv(csv_file)

    # Convert the DataFrame to a numpy array
    bvp_signal_data = df.to_numpy()

    # Ensure the data is a 2D array with shape (1, num_samples)
    bvp_signal_data = bvp_signal_data.reshape(1, -1)

    # Create a BVPsignal object using the numpy array and the sample rate
    bvp_signal = BVPsignal(bvp_signal_data, sample_rate)

    # Calculate the BPM using the getBPM() method
    bpm = bvp_signal.getBPM()

    return bpm
