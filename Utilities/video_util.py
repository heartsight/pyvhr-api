import os
import cv2

def reduce_resolution(input_path, output_path):
    # Open the video file
    video = cv2.VideoCapture(input_path)

    # Get the frame rate of the video
    frame_rate = int(video.get(cv2.CAP_PROP_FPS))

    # Get the video dimensions
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    new_width = int(width / 2)
    new_height = int(height / 2)

    # Define the codec and create a VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_path, fourcc, frame_rate, (new_width, new_height))

    # Loop through the video frames
    while True:
        # Read a frame
        ret, frame = video.read()

        # If the frame was not read successfully, break the loop
        if not ret:
            break

        # Resize the frame to the new resolution
        resized_frame = cv2.resize(frame, (new_width, new_height), interpolation=cv2.INTER_AREA)

        # Write the resized frame to the output video
        out.write(resized_frame)

    # Release the video and VideoWriter objects
    video.release()
    out.release()


def reduce_frames(input_path, output_path):
    # Open the video file
    video = cv2.VideoCapture(input_path)

    # Get the frame rate of the video
    frame_rate = int(video.get(cv2.CAP_PROP_FPS))
    new_frame_rate = 10

    # Get the video dimensions
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # Define the codec and create a VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_path, fourcc, new_frame_rate, (width, height))

    # Define a counter to keep track of frames
    count = 0

    # Loop through the video frames
    while True:
        # Read a frame
        ret, frame = video.read()

        # If the frame was not read successfully, break the loop
        if not ret:
            break

        # Write the frame to the output video if it meets the new frame rate condition
        if count % (frame_rate // new_frame_rate) == 0:
            out.write(frame)

        count += 1

    # Release the video and VideoWriter objects
    video.release()
    out.release()


