import cv2
import numpy as np


def process_environment_landmark(video_path):
    environment = {}

    # Get video duration
    video = cv2.VideoCapture(video_path)
    environment['fps'] = video.get(cv2.CAP_PROP_FPS)  # Frames per second
    frame_count = int(video.get(cv2.CAP_PROP_FRAME_COUNT))  # Total number of frames

    environment['video_duration'] = frame_count / environment['fps']
    environment['bpm_est'] = determine_bpm_est()
    environment['method'] = determine_method(video_path)
    environment['patch_size'] = determine_patch_size(video_path)

    return environment


def process_environment_video(video_path):
    environment = {}

    # Get video duration
    video = cv2.VideoCapture(video_path)
    fps = video.get(cv2.CAP_PROP_FPS)  # Frames per second
    frame_count = int(video.get(cv2.CAP_PROP_FRAME_COUNT))  # Total number of frames

    environment['video_duration'] = frame_count / fps
    environment['roi_approach'] = "holistic"
    environment['bpm_est'] = determine_bpm_est()
    environment['method'] = determine_method(video_path)

    return environment


def determine_method(video_path):
    light_level = estimate_light_level(video_path)
    light_category = categorize_light_level(light_level)

    skin_tone_rgb = estimate_skin_tone(video_path)
    skin_tone_category = categorize_skin_tone(skin_tone_rgb)\

    # Define a dictionary to act as a switch-case
    methods = {
        (1, 1): 'cpu_LGI',
        (1, 2): 'cpu_GREEN',
        (1, 3): 'cpu_PBV',
        (2, 1): 'cpu_PCA',
        (2, 2): 'cpu_CHROM',
        (2, 3): 'cpu_OMIT',
        (3, 1): 'cpu_POS',
        (3, 2): 'cpu_CHROM',
        (3, 3): 'cpu_OMIT'
    }

    # Get the method based on the light and skin tone categories
    method = methods.get((light_category, skin_tone_category))

    # Fall back to a default method if no specific method was found for the given categories
    if method is None:
        method = 'cpu_POS'

    return method


def determine_patch_size(video_path):
    # Load a pre-trained face detector from OpenCV
    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

    # Open the video
    video = cv2.VideoCapture(video_path)
    _, frame = video.read()

    # Convert the frame to grayscale (face detection works better in grayscale)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Detect faces
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))

    if len(faces) != 0:
        # In this example, we only deal with the first detected face
        x, y, w, h = faces[0]

        # Calculate face size
        face_size = np.sqrt(w**2 + h**2)  # Diagonal of the face rectangle

        # Patch size is 10% of face size
        patch_size = int(face_size * 0.1)
    else:
        # No face detected, return a default patch size or handle this situation appropriately
        patch_size = 0

    return patch_size


def determine_bpm_est(context="general"):
    if context == "general":
        return "medians"
    elif context == "specific":
        return "clustering"


def estimate_light_level(video_path):
    video = cv2.VideoCapture(video_path)
    total_intensity = 0
    frame_count = 0

    while True:
        ret, frame = video.read()
        if not ret:
            break

        intensity = np.mean(frame)
        total_intensity += intensity
        frame_count += 1

    average_intensity = total_intensity / frame_count
    return average_intensity


def categorize_light_level(light_level):
    if light_level < 50:
        return 1
    elif light_level < 100:
        return 2
    else:
        return 3


def estimate_skin_tone(video_path):
    # Load a pre-trained face detector from OpenCV
    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

    # Open the video
    video = cv2.VideoCapture(video_path)
    total_color = np.zeros(3)
    frame_count = 0

    while True:
        ret, frame = video.read()
        if not ret:
            break

        # Convert the frame to grayscale for face detection
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Detect faces
        faces = face_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))

        if len(faces) != 0:
            # For this example, we only deal with the first detected face
            x, y, w, h = faces[0]

            # Extract the skin pixels
            skin_pixels = frame[y:y+h, x:x+w]

            # Average the skin pixels
            average_color = np.mean(skin_pixels, axis=(0, 1))

            total_color += average_color
            frame_count += 1

    average_skin_tone = total_color / frame_count
    return average_skin_tone


def categorize_skin_tone(skin_tone_rgb):
    # Calculate the mean of the RGB values
    mean_rgb = np.mean(skin_tone_rgb)

    # Define thresholds for "light", "medium", and "dark"
    if mean_rgb < 85:
        return 1  # "dark"
    elif mean_rgb < 170:
        return 2  # "medium"
    else:
        return 3  # "light"
