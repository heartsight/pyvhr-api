import configparser
import ast
from numpy.lib.arraysetops import isin
import pandas as pd
import numpy as np
from importlib import import_module, util
from pyVHR.datasets.dataset import datasetFactory
from pyVHR.utils.errors import getErrors, printErrors, displayErrors, BVP_windowing
from pyVHR.extraction.sig_processing import *
from pyVHR.extraction.sig_extraction_methods import *
from pyVHR.extraction.skin_extraction_methods import *
from pyVHR.BVP.BVP import *
from pyVHR.BPM.BPM import *
from pyVHR.BVP.methods import *
from pyVHR.BVP.filters import *
from inspect import getmembers, isfunction
import os.path
from pyVHR.deepRPPG.mtts_can import *
from pyVHR.deepRPPG.hr_cnn import *
from pyVHR.extraction.utils import *


class Pipeline:
    """
    This class runs the pyVHR pipeline on a single video or dataset
    """

    minHz = 0.65 # min heart frequency in Hz
    maxHz = 4.0  # max heart frequency in Hz

    def __init__(self):
        pass

    def run_on_landmark(self, sig_processing,
                            sig,
                            winsize,
                            fps,
                            cuda=True,
                            method='cupy_POS',
                            estimate='holistic',
                            movement_thrs=[10, 5, 2],
                            RGB_LOW_HIGH_TH=(75, 230),
                            Skin_LOW_HIGH_TH=(75, 230),
                            pre_filt=False,
                            post_filt=True,
                            verb=True):
        """
        Runs the pipeline on a specific video file.

        Args:
            videoFileName:
                - The video filenane to analyse
            winsize:
                - The size of the window in frame
            ldmks_list:
                - (default None) a list of MediaPipe's landmarks to use, the range is: [0:467]
            cuda:
                - True - Enable computations on GPU
            roi_method:
                - 'convexhull', uses MediaPipe's lanmarks to compute the convex hull on the face skin
                - 'faceparsing', uses BiseNet to parse face components and segment the skin
            roi_approach:
                - 'holistic', uses the holistic approach, i.e. the whole face skin
                - 'patches', uses multiple patches as Regions of Interest
            method:
                - One of the rPPG methods defined in pyVHR
            estimate:
                - if patches: 'medians', 'clustering', the method for BPM estimate on each window
            movement_thrs:
                - Thresholds for movements filtering (eg.:[10, 5, 2])
            patch_size:
                - the size of the square patch, in pixels
            RGB_LOW_HIGH_TH:
                - default (75,230), thresholds for RGB channels
            Skin_LOW_HIGH_TH:
                - default (75,230), thresholds for skin pixel values
            pre_filt:
                - True, uses bandpass filter on the windowed RGB signal
            post_filt:
                - True, uses bandpass filter on the estimated BVP signal
            verb:
                - True, shows the main steps
        """

        av_meths = getmembers(pyVHR.BVP.methods, isfunction)
        available_methods = [am[0] for am in av_meths]

        assert method in available_methods, "\nrPPG method not recognized!!"

        if cuda:
            sig_processing.display_cuda_device()
            sig_processing.choose_cuda_device(0)

        # set sig-processing and skin-processing params
        SignalProcessingParams.RGB_LOW_TH = RGB_LOW_HIGH_TH[0]
        SignalProcessingParams.RGB_HIGH_TH = RGB_LOW_HIGH_TH[1]
        SkinProcessingParams.RGB_LOW_TH = Skin_LOW_HIGH_TH[0]
        SkinProcessingParams.RGB_HIGH_TH = Skin_LOW_HIGH_TH[1]

        sig_processing.set_total_frames(0)

        ## 4. sig windowing
        windowed_sig, timesES = sig_windowing(sig, winsize, 1, fps)
        if verb:
            print(f' - Number of windows: {len(windowed_sig)}')
            print(' - Win size: (#ROI, #landmarks, #frames) = ', windowed_sig[0].shape)

        ## 5. PRE FILTERING
        if verb:
            print('\nPre filtering...')
        filtered_windowed_sig = windowed_sig

        # -- color threshold - applied only with patches
        # if roi_approach == 'patches':
        #    filtered_windowed_sig = apply_filter(windowed_sig,
        #                                        rgb_filter_th,
        #                                        params={'RGB_LOW_TH': RGB_LOW_HIGH_TH[0],
        #                                                'RGB_HIGH_TH': RGB_LOW_HIGH_TH[1]})

        if pre_filt:
            module = import_module('pyVHR.BVP.filters')
            method_to_call = getattr(module, 'BPfilter')
            filtered_windowed_sig = apply_filter(filtered_windowed_sig,
                                                 method_to_call,
                                                 fps=fps,
                                                 params={'minHz': Pipeline.minHz,
                                                         'maxHz': Pipeline.maxHz,
                                                         'fps': 'adaptive',
                                                         'order': 6})
        if verb:
            print(f' - Pre-filter applied: {method_to_call.__name__}')

        ## 6. BVP extraction
        if verb:
            print("\nBVP extraction...")
            print(" - Extraction method: " + method)
        module = import_module('pyVHR.BVP.methods')
        method_to_call = getattr(module, method)

        if 'cpu' in method:
            method_device = 'cpu'
        elif 'torch' in method:
            method_device = 'torch'
        elif 'cupy' in method:
            method_device = 'cuda'

        if 'POS' in method:
            pars = {'fps': 'adaptive'}
        elif 'PCA' in method or 'ICA' in method:
            pars = {'component': 'all_comp'}
        else:
            pars = {}

        bvps_win = RGB_sig_to_BVP(filtered_windowed_sig,
                                  fps, device_type=method_device,
                                  method=method_to_call, params=pars)

        ## 7. POST FILTERING
        if post_filt:
            module = import_module('pyVHR.BVP.filters')
            method_to_call = getattr(module, 'BPfilter')
            bvps_win = apply_filter(bvps_win,
                                    method_to_call,
                                    fps=fps,
                                    params={'minHz': Pipeline.minHz, 'maxHz': Pipeline.maxHz, 'fps': 'adaptive',
                                            'order': 6})
        if verb:
            print(f' - Post-filter applied: {method_to_call.__name__}')

        ## 8. BPM extraction
        if verb:
            print("\nBPM estimation...")


        if estimate == 'clustering':
            # if cuda and False:
            #    bpmES = BVP_to_BPM_PSD_clustering_cuda(bvps_win, fps, minHz=Pipeline.minHz, maxHz=Pipeline.maxHz)
            # else:
            # bpmES = BPM_clustering(sig_processing, bvps_win, winsize, movement_thrs=[15, 15, 15], fps=fps, opt_factor=0.5)
            ma = MotionAnalysis(sig_processing, winsize, fps)
            bpmES = BPM_clustering(ma, bvps_win, fps, winsize, movement_thrs=movement_thrs, opt_factor=0.5)



        elif estimate == 'median':
            if cuda:
                bpmES = BVP_to_BPM_cuda(bvps_win, fps, minHz=Pipeline.minHz, maxHz=Pipeline.maxHz)
            else:
                bpmES = BVP_to_BPM(bvps_win, fps, minHz=Pipeline.minHz, maxHz=Pipeline.maxHz)
            bpmES, _ = BPM_median(bpmES)

            if verb:
                print(f" - BPM estimation with: {estimate}")
        else:
            raise ValueError("Estimation approach unknown!")

        if verb:
            print('\n...done!\n')

        return bvps_win, timesES, bpmES